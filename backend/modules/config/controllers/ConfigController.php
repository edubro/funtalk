<?php

namespace backend\modules\config\controllers;

use Yii;
use common\models\Config;
use common\models\ConfigSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use common\models\UploadForm;
use yii\base\Model;


/**
 * ConfigController implements the CRUD actions for Config model.
 */
class ConfigController extends Controller
{
    /**
     * @inheritdoc
     */
    // public function behaviors()
    // {
    //     return [
    //         'access' => [
    //             'class' => \yii\filters\AccessControl::className(),
    //             'rules' => [
                    
    //                 [
    //                     'actions' => ['create','update','delete', 'index','view'],
    //                     'allow' => true,
    //                     'roles' => ['@'],
    //                 ],
    //             ],
    //         ],
    //         'verbs' => [
    //             'class' => VerbFilter::className(),
    //             'actions' => [
    //                 'delete' => ['POST'],
    //             ],
    //         ],
    //     ];
    // }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index','create','update','delete','view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
    

    /**
     * Lists all Config models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ConfigSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Config model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Config model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Config();
        $uploadModel=new UploadForm();
        if($model->load(Yii::$app->request->post()))
        {
            $uploadModel= UploadedFile::getInstance($model,'imageFile');

            if($uploadModel!=null &&($uploadModel->extension=='jpg'||$uploadModel->extension=='png'||$uploadModel->extension=='ico') && $model->content_type==2)
            {
                $imgName=\Yii::getAlias('@uploads').'/'.$model->key_config.'_'.date('Ymdhis').".".$uploadModel->extension;
                $uploadModel->saveAs($imgName);
                $model->value=$model->key_config.'_'.date('Ymdhis').".".$uploadModel->extension;
            }elseif($uploadModel!=null &&($uploadModel->extension=='mp4'&&$uploadModel->size<=100000000)&& $model->content_type==3)
            {
                $videoName=\Yii::getAlias('@uploads')."/vid_".$model->key_config.'_'.date('Ymdhis').".".$uploadModel->extension;
                $uploadModel->saveAs($videoName);
                $model->value="vid_".$model->key_config.'_'.date('Ymdhis').".".$uploadModel->extension;
            }

            if ( $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }else{
                return $this->render('create', [
                    'model' => $model,
                ]);
            } 
        }else {
                return $this->render('create', [
                    'model' => $model,
                ]);
        }
    }



    /**
     * Updates an existing Config model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $uploadModel=new UploadForm();

        if ($model->load(Yii::$app->request->post()) ) {
            
            $uploadModel= UploadedFile::getInstance($model,'imageFile');
            
            if($uploadModel!=null &&($uploadModel->extension=='jpg'||$uploadModel->extension=='png'||$uploadModel->extension=='ico')&& $model->content_type==2)
            {
                $imgName="../../frontend/web/uploads/".$model->key_config.'_'.date('Ymdhis').".".$uploadModel->extension;
                $uploadModel->saveAs($imgName);
                $model->value="uploads/".$model->key_config.'_'.date('Ymdhis').".".$uploadModel->extension;
            }elseif($uploadModel!=null &&($uploadModel->extension=='mp4'&&$uploadModel->size<=100000000)&& $model->content_type==3)
            {
                $videoName="../../frontend/web/uploads/vid_".$model->key_config.'_'.date('Ymdhis').".".$uploadModel->extension;
                $uploadModel->saveAs($videoName);
                $model->value="uploads/vid_".$model->key_config.'_'.date('Ymdhis').".".$uploadModel->extension;
            }

            if($model->save())
            {
                return $this->redirect(['view', 'id' => $model->id]);    
            }else{
                return $this->render('update', [
                    'model' => $model,
                    ]);    
            }
            
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Config model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Config model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Config the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Config::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
