<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ConfigSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Konfigurasi';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="config-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Buat Konfigurasi', ['create'], ['class' => 'btn btn-flat btn-success']) ?>
    </p>
    <?= fedemotta\datatables\DataTables::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'key_config',
            'value:ntext',
            'configuration_type',
            'content_type',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
