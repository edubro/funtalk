<?php

namespace backend\modules\member\controllers;

use Yii;
use common\models\Member;
use common\models\MemberSearch;
use common\models\UploadForm;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;


/**
 * AppController implements the CRUD actions for Member model.
 */
class AppController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
        'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index','create','update','delete','view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Member models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MemberSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Member model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Member model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Member();
        $uploadModel=new UploadForm();
        if($model->load(Yii::$app->request->post()))
        {
            $uploadModel= UploadedFile::getInstance($model,'imageFile');

            if($uploadModel!=null &&($uploadModel->extension=='jpg'||$uploadModel->extension=='png'))
            {
                $imgName=\Yii::getAlias('@uploads').'/'.$model->name.'_'.date('Ymdhis').".".$uploadModel->extension;
                $uploadModel->saveAs($imgName);
                $model->photo=$model->name.'_'.date('Ymdhis').".".$uploadModel->extension;
            }

            if ( $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }else{
                return $this->render('create', [
                    'model' => $model,
                ]);
            } 
        }else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Member model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
         if($model->load(Yii::$app->request->post()))
        {
            $uploadModel= UploadedFile::getInstance($model,'imageFile');

            if($uploadModel!=null &&($uploadModel->extension=='jpg'||$uploadModel->extension=='png'))
            {
                $imgName=\Yii::getAlias('@uploads').'/'.$model->name.'_'.date('Ymdhis').".".$uploadModel->extension;
                $uploadModel->saveAs($imgName);
                $model->photo=$model->name.'_'.date('Ymdhis').".".$uploadModel->extension;
            }

            if ( $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }else{
                return $this->render('create', [
                    'model' => $model,
                ]);
            } 
        }else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Member model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Member model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Member the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Member::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
