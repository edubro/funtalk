<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\view;
use yii\web\Request;

/* @var $this yii\web\View */
/* @var $model common\models\Config */
/* @var $form yii\widgets\ActiveForm */
$configType = \common\models\ConfigurationType::find()->asArray()
        ->all();

$configTypeArray = yii\helpers\ArrayHelper::map(
        $configType, 'id', 'name');


$selected_content_type=1;
if(isset($_GET['content_type']))
{

 $selected_content_type=$_GET['content_type'];

}else{

    if($model->content_type==2)
    {
        $selected_content_type=2;
    }elseif($model->content_type==3){
        $selected_content_type=3;
    }
}

?>



<div class="config-form">

    <?php $form = ActiveForm::begin([
            'options'=>[
                'enctype'=>'multipart/form-data',
            ],
        ]); ?>

    <?= $form->field($model, 'content_type')->dropDownList(\common\models\Constant::CONTENT_TYPE_CONFIG) ?>
    <?= $form->field($model, 'configuration_type')->dropDownList($configTypeArray) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'key_config')->textInput(['maxlength' => true]) ?>
    <?php if($model->value!=''&&$model->content_type==2): ?>
    <div class="form-group">
        <label class="control-label" ></label>

        <img src=" <?=Yii::$app->urlManager->createUrl('../frontend/web/uploads/'.$model->value)?>" style="border:1px solid black;max-width:500px;">

        <div class="help-block"></div>
    </div>
    <?php endif; ?>
    <?php if($selected_content_type==2): ?>
    <?= $form->field($model, 'imageFile')->label('Image')->fileInput() ?>
    <?php elseif($selected_content_type==3): ?>
    <?= $form->field($model, 'imageFile')->label('Video')->fileInput() ?>
    <?php endif; ?>
    <?= $form->field($model, 'order_config')->textInput(['type'=>'number']) ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


</script>

<?php 



$myJS="
$(document).ready(function(){
    $('#config-content_type').val('".$selected_content_type."');

    $('#config-content_type').change(function(){
             if($(this).val()==1)
            {
                if(window.location.href.indexOf('update')>=0)
                {
                    window.location='".\yii\helpers\Url::to(['config/update','id'=>$model->id,'content_type'=>1])."';      
                }else{
                    window.location='".\yii\helpers\Url::to(['config/create'])."';  
                }
            }
            else if($(this).val()==2)
            {
                if(window.location.href.indexOf('update')>=0)
                {
                    window.location='".\yii\helpers\Url::to(['config/update','id'=>$model->id,'content_type'=>2])."';
                }else{
                    window.location='".\yii\helpers\Url::to(['config/create','content_type'=>2])."';
                }
            }else if($(this).val()==3)
            {
                if(window.location.href.indexOf('update')>=0)
                {
                    window.location='".\yii\helpers\Url::to(['config/update','id'=>$model->id,'content_type'=>3])."';
                }else{
                    window.location='".\yii\helpers\Url::to(['config/create','content_type'=>3])."';
                }
            }
            else if($(this).val()==4)
            {
                if(window.location.href.indexOf('update')>=0)
                {
                    window.location='".\yii\helpers\Url::to(['config/update','id'=>$model->id,'content_type'=>4])."';
                }else{
                    window.location='".\yii\helpers\Url::to(['config/create','content_type'=>4])."';
                }
            }
    });
});

";
$this->registerJs($myJS,yii\web\View::POS_READY);
 ?>