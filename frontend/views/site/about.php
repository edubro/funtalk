
<div style="overflow:hidden;">
<div id="header">
    <header class="header" style="position: relative;"><!--header-start-->
      <div id="bg-opacity-header" style="position: absolute;width: 100%;height: 100%;background-color: rgba(0, 0, 0, 0.7);z-index: 1; "></div>
      <div class="container-fluid img-opacity-header" style="height: 100%; width: 100%;">
            <div id="myCarousel" class="carousel slide" data-ride="carousel" style="height: 90vh; width: 100%;">
  			<!-- Indicators -->
  			<ol class="carousel-indicators">
  			   <?php 
                    foreach ($banner as $key => $bn) {
                 ?>
                    <?php if($key==0){?>
                        <li data-target="#myCarousel" data-slide-to="<?php echo $key; ?>" class="active"></li>
                    <?php }else{?>
                        <li data-target="#myCarousel" data-slide-to="<?php echo $key; ?>"></li>
                    <?php } ?>

                 <?php } ?>
  			</ol>

  			<!-- Wrapper for slides -->
  			<div class="carousel-inner" role="listbox" style="height: 100%; width: 100%;">

  			  <?php 
                    foreach ($banner as $key => $bn) {
                 ?>
                    <?php if($key==0){?>
                     <div class="item active">
                        <img src="<?=Yii::$app->urlManager->createUrl('/uploads/'.$bn->image)?>" alt="">
                    </div>
                    <?php }else{?>
                    <div class="item">
                        <img src="<?=Yii::$app->urlManager->createUrl('/uploads/'.$bn->image)?>" alt="">
                    </div>      
                    <?php } ?>

                 <?php } ?>
  		  
  			</div>
  		  </div>		  
    </div>
    <div>
      <div class="caption">
      <?php echo $caption->value; ?>
      </div>
    </div>
	</header><!--header-end-->
</div>

<section class="main-section" id="service"><!--main-section-start-->
	<div class="container">
    	<h2 class="text-padding-h2 wow fadeInUp delay-02s text-orange"><?php echo $content1_title->value; ?></h2>
		<h6 class="text-padding-h2 wow fadeInUp delay-04s text-black">
      <?php echo $content1_text->value; ?>
    </h6>
		<br>
		<div class="col-md-6 col-xs-6 no-padding-left">
			<h3 class="text-padding-h2 wow fadeInUp delay-06s text-orange"><?php echo $content2_title->value; ?></h3>
      <small class="text-padding-h2 wow fadeInUp delay-08s text-black"><?php echo $content2_text->value; ?></small>     
		</div>
		<div class="col-md-6 col-xs-6 no-padding-right">
			<h3 class="text-padding-h2 wow fadeInUp delay-1s text-orange"><?php echo $content3_title->value; ?></h3>
			<small class="text-padding-h2 wow fadeInUp delay-12s text-black"><?php echo $content3_text->value; ?></small>		
		</div>		
	</div>
</section><!--main-section-end-->


<section class="main-section paddind" id="Portfolio">
	<div class="container">
		<h2 class="text-padding-h2 wow fadeInUp delay-02s text-white"><?php echo $content4_title->value; ?></h2>
    <div id="about-bottom-icon">
	     <?php echo $content4_text->value; ?>
	  </div>
  </div>
</section>
