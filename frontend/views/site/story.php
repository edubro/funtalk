
<div id="header">
    <header class="header" style="position: relative;"><!--header-start-->
      <div id="bg-opacity-header" style="position: absolute;width: 100%;height: 100%;background-color: rgba(0, 0, 0, 0.7);z-index: 1; "></div>
      <div class="container-fluid img-opacity-header" style="height: 100%; width: 100%;">
            <div id="myCarousel" class="carousel slide" data-ride="carousel" style="height: 90vh; width: 100%;">
  			<!-- Indicators -->
  			<ol class="carousel-indicators">
  			  <?php 
                    foreach ($banner as $key => $bn) {
                 ?>
                    <?php if($key==0){?>
                        <li data-target="#myCarousel" data-slide-to="<?php echo $key; ?>" class="active"></li>
                    <?php }else{?>
                        <li data-target="#myCarousel" data-slide-to="<?php echo $key; ?>"></li>
                    <?php } ?>

                 <?php } ?>
  			</ol>

  			<!-- Wrapper for slides -->
  			<div class="carousel-inner" role="listbox" style="height: 100%; width: 100%;">

  			  <?php 
                    foreach ($banner as $key => $bn) {
                 ?>
                    <?php if($key==0){?>
                     <div class="item active">
                        <img src="<?=Yii::$app->urlManager->createUrl('/uploads/'.$bn->image)?>" alt="">
                    </div>
                    <?php }else{?>
                    <div class="item">
                        <img src="<?=Yii::$app->urlManager->createUrl('/uploads/'.$bn->image)?>" alt="">
                    </div>      
                    <?php } ?>

                 <?php } ?>
  			</div>
  		  </div>		  
    </div>
    <div>
      <div class="caption">
      <?php echo $caption->value; ?>
      </div>
    </div>
	</header><!--header-end-->
</div>
</div>

<section class="main-section story" id="Portfolio"><!--main-section-start-->
	<div class="container">
	<h2 class="text-padding-h2 text-orange wow fadeInUp delay-02s"><?php echo $content_title->value; ?></h2>	
	
		<?php foreach ($news as $key => $nws) {?>
            <?php $image=Yii::$app->urlManager->createUrl('/uploads/'.$nws->banner_image); ?>
            <div class="col-md-4 col-xs-12 no-padding-left">
            <a href="<?php echo \Yii::$app->urlManager->createUrl('news/'.$nws->id.'/'.$nws->title); ?>"><div class="image-photo bg-photo-1" style="background:url('<?php echo $image; ?>')"></div></a>
            <h3 class="text-black text-capitalize"><?php echo $nws->title; ?></h3>
            <h6 class="text-black text-capitalize text-left">Oleh : <?php echo $nws->writer; ?></h6>
            <!-- <i class="fa fa-facebook fa-fb" aria-hidden="true"></i>
            <i class="fa fa-twitter fa-tw" aria-hidden="true"></i>
            <i class="fa fa-whatsapp fa-wa" aria-hidden="true"></i>
            <i class="fa fa-pinterest fa-pin" aria-hidden="true"></i>
            <i class="fa fa-envelope" aria-hidden="true"></i> -->
            <?php if(($key+1)%3==0){ ?>
                <div class="clearfix"></div> <br/><br/><br/>
            <?php } ?>
        </div>

        <?php } ?>		
	</div>
</section>