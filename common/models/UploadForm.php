<?php

namespace common\models;

use yii\base\Model;
use yii\web\UploadedFile;
use yii\helpers\BaseHtml;

class UploadForm extends Model {

    /**
     * @var UploadedFile
     */
    public $imageFile;

    public function rules() {
        return [
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],
        ];
    }

    public function upload() {
        if (gettype($this->imageFile) === 'array' && count($this->imageFile) > 0) {
            $this->imageFile = $this->imageFile[0];
            
        }
        
        if ($this->validate()) {
            $name = $this->imageFile->baseName . '.' . $this->imageFile->extension;
            $this->imageFile->saveAs('uploads/' . $name);
            return $name;
        } else {
            return false;
        }
    }

}
