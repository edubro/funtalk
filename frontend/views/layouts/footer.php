

<footer class="footer">
    <div class="container">
    <div class="col-md-3 col-xs-4 text-left">
      <span class="copyright">Daftar Untuk Berlangganan</span>
      <div class="form-group">
        <input type="text" style="font-size: 67%" class="form-control input-black" id="alamat_email" placeholder="Alamat Email">
      </div>      
      <br/>
      <p>Powered by Mamorasoft</p>
    </div>
    <div class="col-md-4 col-xs-5">
      <span class="copyright">Hubungi Kami</span> 
        <?php $contact= \common\models\Config::findOne(['key_config'=>'g_contact']);

          echo $contact->value;
         ?>     
    </div>    
    <div class="col-md-5 col-xs-12" align="center">
      <span class="copyright">Ikuti Kami</span> 
      <iframe src="<?php $g_instagram= \common\models\Config::findOne(['key_config'=>'g_instagram']);

          echo $g_instagram->value;
         ?> " width="100%" height="200px"></iframe>     
    </div>      
    </div>
    <!-- 
        All links in the footer should remain intact. 
        Licenseing information is available at: http://bootstraptaste.com/license/
        You can buy this theme without footer links online at: http://bootstraptaste.com/buy/?theme=Knight
    -->
</footer>

<?php 

 ?>