<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "program".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $image
 * @property integer $display_status
 * @property integer $order_program
 */
class Program extends \yii\db\ActiveRecord
{
    public $imageFile;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'program';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description', 'image', 'display_status', 'order_program'], 'required'],
            [['description', 'image'], 'string'],
            [['display_status', 'order_program'], 'integer'],
            [['name'], 'string', 'max' => 100],
            [['imageFile'],'file'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'image' => 'Image',
            'display_status' => 'Display Status',
            'order_program' => 'Order Program',
        ];
    }
}
