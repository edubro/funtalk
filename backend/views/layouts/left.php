<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left info">

            </div>
        </div>

        <!-- search form -->
        <!-- <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form> -->
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => 'Menu', 'options' => ['class' => 'header']],
                    ['label' => 'Website Content', 'icon' => 'gear', 'url' => ['/config/app']],
                    ['label' => 'Berita Kami', 'icon' => 'newspaper-o', 'url' => ['/news/app']],
                    ['label' => 'Member', 'icon' => 'user', 'url' => ['/member/app']],
                    ['label' => 'Banner', 'icon' => 'image', 'url' => ['/banner/app']],
                    ['label' => 'Program', 'icon' => 'book', 'url' => ['/program/app']],
                    ['label' => 'NewsLetter', 'icon' => 'envelope', 'url' => ['/news/news-letter']],



                ],
            ]
        ) ?>

    </section>

</aside>
