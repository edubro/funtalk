<?php

namespace frontend\controllers;
use Yii;
use common\models\Program;
use common\models\Banner;
use common\models\Config;
use common\models\News;
use common\models\Member;


class NewsController extends \yii\web\Controller
{
    public function actionIndex($id,$title)
    {

    	$banner=Banner::find()
    	->where(['configuration_type'=>1])
      ->orWhere(['configuration_type'=>9])
    	->orderBy(['order_banner'=>SORT_ASC])
    	->all();
    	$caption=Config::findOne(['key_config'=>'bdt_caption']);
    	$content_title=Config::findOne(['key_config'=>'bdt_content_title']);

    	$ot_news=News::find()
              ->where(['display_status'=>1])
              ->orderBy(['date_created'=>SORT_DESC])
              ->limit(3)
              ->all();
        $news=News::find()
              ->where(['id'=>$id])
              ->one();
        $this->view->title = Config::findOne(['key_config'=>'g_title'])->value.'|'.$news->title;

        if($news!=null)
        {
        	$news->view=$news->view+1;
        	$news->last_view=date('Y-m-d H:i:s');
        	$news->save();
        }

        return $this->render('index',[
        		'banner'=>$banner,
        		'news'=>$news,
        		'ot_news'=>$ot_news,
        		'caption'=>$caption,
        		'content_title'=>$content_title,
        	]);
    }

}
