<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $searchModel common\models\NewsLetterSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'News Letters';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-letter-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create News Letter', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= fedemotta\datatables\DataTables::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'email:email',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    
</div>
