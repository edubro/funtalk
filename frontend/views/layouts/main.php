<?php 
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;

AppAsset::register($this);


 ?>
 <?php $this->beginPage() ?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, maximum-scale=1">
<?= Html::csrfMetaTags() ?>
<?php $icon=common\models\Config::findOne(['key_config'=>'g_icon']); ?>
<link rel="shortcut icon" href="<?php echo Yii::$app->urlManager->createUrl('/uploads/'.$icon->value); ?>" type="img/x-icon">


<title><?= Html::encode($this->title) ?></title>
<!-- <link rel="icon" href="favicon.png" type="image/png"> -->
<!-- <link rel="shortcut icon" href="favicon.ico" type="img/x-icon">
 -->
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,800italic,700italic,600italic,400italic,300italic,800,700,600' rel='stylesheet' type='text/css'>
<?php $this->head() ?>


<!--[if lt IE 9]>
    <script src="js/respond-1.1.0.min.js"></script>
    <script src="js/html5shiv.js"></script>
    <script src="js/html5element.js"></script>
<![endif]-->


</head>
<body>
<?php $this->beginBody() ?>
<nav class="navbar navbar-default navbar-no-padding">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false" style="margin-top: 20px; margin-bottom: -20px;">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <?php $logo=common\models\Config::findOne(['key_config'=>'g_logo']); ?>
      <a class="navbar-brand" href="<?= \yii\helpers\Url::to(['site/index']) ?>"><img src="<?php echo Yii::$app->urlManager->createUrl('/uploads/'.$logo->value); ?>"/></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="margin-top: 20px;">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="<?= \yii\helpers\Url::to(['site/index']) ?>">Beranda</a></li>
        <li class="dropdown">
          <a href="about.html" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Tentang Kami <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="<?= \yii\helpers\Url::to(['site/about']) ?>">Budaya</a></li>
            <li><a href="<?= \yii\helpers\Url::to(['site/tim-kami']) ?>">Tim Kami</a></li>
            <li><a href="<?= \yii\helpers\Url::to(['site/partner']) ?>">Partner</a></li>
            <li><a href="<?= \yii\helpers\Url::to(['site/publikasi']) ?>">Publikasi</a></li>
          </ul>
        </li>
        <li><a href="<?= \yii\helpers\Url::to(['site/cerita-kami']) ?>">Cerita Kami</a></li>		
        <li><a href="<?=\yii\helpers\Url::to(['site/funlabs'])?>">FunLabs Indonesia</a></li>		
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

<?= $content ?>
<?= $this->render("footer"); ?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
<script type="text/javascript">
$(document).ready(function(e) {
  $('#test').scrollToFixed();
  $('.res-nav_click').click(function(){
    $('.main-nav').slideToggle();
    return false    

  });

});
</script>

<script>
wow = new WOW(
{
  animateClass: 'animated',
  offset:       100
}
);
wow.init();

</script>


<script type="text/javascript">
$(window).load(function(){

  $('.main-nav li a').bind('click',function(event){
    var $anchor = $(this);

    $('html, body').stop().animate({
      scrollTop: $($anchor.attr('href')).offset().top - 102
    }, 1500,'easeInOutExpo');
      /*
      if you don't want to use the easing effects:
      $('html, body').stop().animate({
        scrollTop: $($anchor.attr('href')).offset().top
      }, 1000);
  */
  event.preventDefault();
});
})
</script>

<script type="text/javascript">

$(window).load(function(){


  var $container = $('.portfolioContainer'),
  $body = $('body'),
  colW = 375,
  columns = null;

  
  $container.isotope({
    // disable window resizing
    resizable: true,
    masonry: {
      columnWidth: colW
    }
  });
  
  $(window).smartresize(function(){
    // check if columns has changed
    var currentColumns = Math.floor( ( $body.width() -30 ) / colW );
    if ( currentColumns !== columns ) {
      // set new column count
      columns = currentColumns;
      // apply width to container manually, then trigger relayout
      $container.width( columns * colW )
      .isotope('reLayout');
    }
    
  }).smartresize(); // trigger resize to set container width
  $('.portfolioFilter a').click(function(){
    $('.portfolioFilter .current').removeClass('current');
    $(this).addClass('current');

    var selector = $(this).attr('data-filter');
    $container.isotope({

      filter: selector,
    });
    return false;
  });
  
});

</script>
<script type="text/javascript">
<?php if(Yii::$app->session->hasFlash("success")): ?>
alert('<?php echo Yii::$app->session->getFlash("success"); ?>');
<?php Yii::$app->session->removeFlash("success"); ?>
<?php endif; ?>

</script>
