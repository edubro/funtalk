-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 15, 2018 at 09:26 AM
-- Server version: 10.1.24-MariaDB-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `funtalki_db_funtalk`
--

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE `banner` (
  `id` int(11) NOT NULL,
  `image` text NOT NULL,
  `configuration_type` int(11) DEFAULT NULL,
  `order_banner` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`id`, `image`, `configuration_type`, `order_banner`) VALUES
(9, '1_20170524035949.jpg', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

CREATE TABLE `config` (
  `id` int(11) NOT NULL,
  `key_config` varchar(255) NOT NULL,
  `name` varchar(100) NOT NULL,
  `value` text NOT NULL,
  `configuration_type` int(11) NOT NULL,
  `content_type` tinyint(1) NOT NULL COMMENT '1:text, 2:image',
  `order_config` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `config`
--

INSERT INTO `config` (`id`, `key_config`, `name`, `value`, `configuration_type`, `content_type`, `order_config`) VALUES
(1, 'g_logo', 'Logo', 'g_logo_20170428051039.png', 1, 2, 1),
(2, 'g_title', 'Title', 'Fun Talk ', 1, 4, 0),
(3, 'g_contact', 'Contact', '<p>E: funtalkindonesia@gmail.com</p><p>A: Jalan Setia Budi NO. 464 Tj. Sari, Medan Indonesia.</p>', 1, 1, 2),
(4, 'bdy_caption', 'Caption', '<h3>ABOUT</h3>', 2, 4, 1),
(5, 'brd_caption', 'Caption', '<h3>Kami Percaya</h3>\r\n<p>Perubahan Tidaklah Harus Datang Dari Hal Yang Besar</p><br>', 3, 4, 0),
(6, 'g_instagram', 'Instagram', 'https://www.instagram.com/p/BUEEKOjgKqt/embed/', 1, 4, 3),
(7, 'bdy_content1_title', 'Content 1 Title', 'SIAPA KAMI', 2, 4, 2),
(8, 'bdy_content1_text', 'Content 1 Text', '<p>Fun Talk Indonesia terbentuk pada bulan november 2014, sebagai perkumpulan sosial yang ditujukan untuk membantu penyebaran inspirasi yang dikemas dengan berbeda, menyenangkan, dan menarik, serta dapat menyelesaikan permasalahan serta meningkatkan kesadaran akan hal-hal yang dianggap benar. Dengan mempercayai kekuatan untuk saling menginspirasi yang dapat merubah sikap, kehidupan, juga lingkungan.</p>', 2, 1, 4),
(9, 'bdy_content2_title', 'Content 2 Title', 'VISI KAMI', 2, 4, 5),
(10, 'bdy_content2_text', 'Content 2 Text', 'Menjadi wadah untuk jutaan inspirasi dari berbagai macam pemikir serta komunitas lainnya yang turut berpartisipasi agar tetap bermanfaat satu dengan yang lainnya melalui kegiatan-kegiatan FunTalk.\r\n\r\n', 2, 4, 6),
(11, 'bdy_content3_title', 'Content 3 Title', 'MISI KAMI', 2, 4, 7),
(12, 'bdy_content3_text', 'Content 3 Text', 'Menjadi wadah untuk jutaan inspirasi dari berbagai macam pemikir serta komunitas lainnya yang turut berpartisipasi agar tetap bermanfaat satu dengan yang lainnya melalui kegiatan-kegiatan FunTalk.', 2, 4, 8),
(13, 'bdy_content4_title', 'Content 4 Title', '<p>CARA KAMI</p>', 2, 1, 9),
(14, 'bdy_content4_text', 'Content 4 Text', '<h6 class=\"text-padding-h2 wow fadeInUp delay-04s text-white animated\" style=\"font-family: Montserrat-Medium, sans-serif; line-height: 30px; color: rgb(255, 255, 255); margin-bottom: 20px; font-size: 15px; animation-delay: 0.4s; animation-duration: 1s; animation-fill-mode: both; animation-name: fadeInUp; visibility: visible;\">Kami bersinergi dengan para inspirator penggerak perubahan untuk dapat menginspirasi banyak orang sehingga dapat menciptakan inspirator-inspirator lainnya untuk Indonesia yang lebih baik.</h6><h6 class=\"text-padding-h2 wow fadeInUp delay-04s text-white animated\" style=\"font-family: Montserrat-Medium, sans-serif; line-height: 30px; color: rgb(255, 255, 255); margin-bottom: 20px; font-size: 15px; animation-delay: 0.4s; animation-duration: 1s; animation-fill-mode: both; animation-name: fadeInUp; visibility: visible;\"><p class=\"col-md-4 col-xs-4 no-padding-left\" align=\"center\" style=\"padding-left: 0px; width: 380px; font-family: Montserrat, sans-serif;\"><img src=\"/funtalk\\uploads/guest/956936879f-about-1.png\" class=\"wow fadeInUp delay-06s animated\" draggable=\"true\" data-bukket-ext-bukket-draggable=\"true\"> <br><small class=\"text-padding-h2 wow fadeInUp delay-12s text-white animated\">Mengumpulkan Inspirator para penggerak perubahan.</small></p><p class=\"col-md-4 col-xs-4 no-padding\" align=\"center\" style=\"width: 380px; font-family: Montserrat, sans-serif;\"><img src=\"/funtalk\\uploads/guest/cedb0f1d9c-about-2.png\" class=\"wow fadeInUp delay-08s animated\" draggable=\"true\" data-bukket-ext-bukket-draggable=\"true\"> <br><small class=\"text-padding-h2 wow fadeInUp delay-12s text-white animated\">Menghubungkan mereka dengan minat yang sama.</small></p><p class=\"col-md-4 col-xs-4 no-padding-right\" align=\"center\" style=\"padding-right: 0px; width: 380px; font-family: Montserrat, sans-serif;\"><img src=\"/funtalk\\uploads/guest/73fed7fd47-about-3.png\" class=\"wow fadeInUp delay-1s animated\" draggable=\"true\" data-bukket-ext-bukket-draggable=\"true\"> <br><small class=\"text-padding-h2 wow fadeInUp delay-12s text-white animated\">Menciptakan kolaborasi untuk saling menginspirasi</small></p><p class=\"col-md-4 col-xs-4 no-padding-right\" align=\"center\" style=\"padding-right: 0px; width: 380px; font-family: Montserrat, sans-serif;\"><small class=\"text-padding-h2 wow fadeInUp delay-12s text-white animated\"></small></p></h6>', 2, 1, 10),
(15, 'tk_caption', 'Caption', '<h3>Team</h3>', 4, 4, 0),
(16, 'tk_content_title', 'Content Title', 'Tim Kami', 4, 4, 1),
(17, 'tk_content_text', 'Content Text', '.', 4, 4, 2),
(18, 'pt_caption', 'Caption', '<h3>Friends</h3>', 5, 4, 0),
(19, 'pt_content_title', 'Content Title', 'VOLUNTEER KECE', 5, 4, 1),
(20, 'pt_content_text', 'Content Text', '.', 5, 4, 2),
(21, 'pub_caption', 'Caption', '<h3>archive</h3>\r\n', 6, 4, 0),
(22, 'pub_content_title', 'Content Title', '.', 6, 4, 1),
(23, 'ck_caption', 'Caption', '<h3>stories</h3>', 7, 4, 0),
(24, 'ck_content_title', 'Content Title', 'CERITA KAMI', 7, 4, 1),
(25, 'bdt_caption', 'Caption', '<h3>stories</h3>', 9, 4, 0),
(26, 'bdt_content_title', 'Content Title', 'CERITA KAMI', 9, 4, 1),
(27, 'flb_caption', 'Caption', '<h1 class=\"title-left-header\">Funlabs</h1>\r\n<h4 class=\"title-left\">Jalan Setia Budi NO. 464 Tj. Sari, Medan Indonesia.</h4>', 8, 4, 0),
(28, 'flb_logo', 'Logo', 'flb_logo_20180212070408.png', 8, 2, 1),
(29, 'g_icon', 'Icon', 'g_icon_20170428045159.png', 1, 2, 1),
(30, 'flb_address', 'Address', 'Jalan Setia Budi NO. 464 Tj. Sari, Medan Indonesia', 8, 4, 2);

-- --------------------------------------------------------

--
-- Table structure for table `configuration_type`
--

CREATE TABLE `configuration_type` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `order_config_type` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `configuration_type`
--

INSERT INTO `configuration_type` (`id`, `name`, `order_config_type`) VALUES
(1, 'General', 1),
(2, 'Budaya', 2),
(3, 'Beranda', 1),
(4, 'Tim Kami', 4),
(5, 'Partner', 5),
(6, 'Publikasi', 6),
(7, 'Cerita Kami', 7),
(8, 'FunLabs Indonesia', 11),
(9, 'Berita Detail', 12);

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `position` varchar(150) NOT NULL,
  `description` text NOT NULL,
  `photo` text,
  `status` tinyint(2) NOT NULL COMMENT '1=Member,2=Partner',
  `facebook` text,
  `twitter` text,
  `whatsapp` text,
  `pinterest` text,
  `email` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`id`, `name`, `position`, `description`, `photo`, `status`, `facebook`, `twitter`, `whatsapp`, `pinterest`, `email`) VALUES
(3, 'Jimmy', '.', '<p>ceritajimmy.id/</p>\r\n<p>instagram.com/ceritajimmy/</p>', 'Jimmy_20170506110607.png', 1, '', '', '', '', ''),
(4, 'togar', '.', '<p>instagram.com/togarmanik</p>', 'togar_20170506110948.png', 1, '', '', '', '', ''),
(5, 'tosi', '.', '<p>instagram.com/tosima_saragih</p>', 'tosi_20170506111123.png', 1, '', '', '', '', ''),
(6, 'bernard', '.', '<p>instagram.com/bernardadt</p>', 'bernard_20170506111246.png', 1, '', '', '', '', ''),
(7, 'yuni', '.', '<p>ahyunimiwa.wordpress.com/</p>\r\n<p>instagram.com/ahyunimiwa</p>', 'yuni_20170506111416.png', 1, '', '', '', '', ''),
(8, 'dhian', '.', '<p>instagram.com/dhilywp</p>', 'dhian_20170524023837.png', 1, '', '', '', '', ''),
(9, 'Dosni', '.', '<p>instagram.com/</p>', '45t45t4_20170513112406.png', 1, '', '', '', '', ''),
(11, 'Agoez Perdana', ' Editor in Chief Kabarmedan.com', '<p>.</p>', 'Agoez Perdana_20170526122235.png', 2, '', '', '', '', ''),
(19, 'Vito Sinaga', 'Clothing Punya Medan', '<p>.</p>', 'Vito Sinaga_20170526122519.png', 2, '', '', '', '', ''),
(20, 'Relcky Saragih', 'Career Coach & Consultant', '<p>.</p>', 'Relcky Saragih_20170526122749.png', 2, '', '', '', '', ''),
(21, 'Windi Septia Dewi', 'Owner Teri Bajak Medan', '<p>.</p>', 'Windi Septia Dewi_20170526122959.png', 2, '', '', '', '', ''),
(22, 'Mustika Yuliandri', 'Coffee Lover & Social Media Enthusiast', '<p>.</p>', 'Mustika Yuliandri_20170526123410.png', 2, '', '', '', '', ''),
(23, 'Irfan Marpaung', 'Warung Seni Kopi', '<p>.</p>', 'Irfan Marpaung_20170526123705.png', 2, '', '', '', '', ''),
(24, 'BEby chubby', 'Announcer', '<p>.</p>', 'BEby chubby_20170526123956.png', 2, '', '', '', '', ''),
(25, 'Desi Yani Harahap', 'Warung Baca Bahagia', '<p>.</p>', 'Desi Yani Harahap_20170526124252.png', 2, '', '', '', '', ''),
(26, 'Rizky Nasution', 'Founder Medan Heritage', '<p>.</p>', 'Rizky Nasution_20170526124652.png', 2, '', '', '', '', ''),
(27, 'adit', '.', '<p>instagram.com/taukotembung</p>', 'adit_20170530064117.png', 1, '', '', '', '', ''),
(28, 'Eko', '.', '<p>instagram.com/ekororore</p>', 'Eko_20170526013422.png', 1, '', '', '', '', ''),
(29, 'Daud', '.', '<p>instagram.com/</p>', 'Daud_20170526013504.png', 1, '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1493095424),
('m130524_201442_init', 1493095430);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `banner_image` text NOT NULL,
  `date_created` datetime DEFAULT CURRENT_TIMESTAMP,
  `date_update` datetime DEFAULT CURRENT_TIMESTAMP,
  `view` int(11) NOT NULL DEFAULT '0',
  `last_view` datetime DEFAULT NULL,
  `display_status` tinyint(1) NOT NULL DEFAULT '1',
  `writer` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `title`, `content`, `banner_image`, `date_created`, `date_update`, `view`, `last_view`, `display_status`, `writer`) VALUES
(6, 'Komunitas Fun Talk Indonesia Gelar Aksi Bersih Istana Maimoon', '<p><span style=\"font-size: 18px;\">KabarMedan.com | Banyak komunitas yang dibentuk oleh anak­anak muda di Indonesia khusunya Medan .Komunitas yang dibentuk karena hobby misalnya, seperti fotografi, Sepeda, Dance, musik atau ada juga komunitas dalam bidang olahraga, komunitas yang perduli dengan lingkungan atau komunitas berbasis kewirausahaan dan masih banyak yang lainnya. Satu lagi Komunitas positif di Medan yaitu FunTalk Indonesia. Komunitas ini berdiri di Medan November 2014 yang lalu. Komunitas yang punya tagline “Bersama Kita Pintar “ ini baru memulai aktifitasnya di awal tahun 2015 ini. “Komunitas ini bukan komunitas biasa, kami berkumpul bersama untuk melakukan hal hal kecil untuk sebuah perubahan yang besar, itu adalah makna dari Bersama Kita Pintar, dan mengapa kami megusung kata Indonesia, karena mimpi terbesar kami adalah akan ada FunTalk di setiap kota besar diseluruh Indonesia dan yang terpenting kami,kita melakukannya dengan Fun!,” kata Jimmy, selaku Penggagas Komunitas FunTalk Indonesia. <br><br>Pada tanggal 31 Januari 2015 yang lalu, FunTalk Indonesia melakukan aksi #SabtuBersih di lingkungan Istana Maimoon Medan. Mengapa Istana Maimoon dan mengapa mesti hari Sabtu?. Komunitas FunTalk Indonesia Telkomsel Dukung Event Karnaval Kemerdekaan Pesona Danau Toba 2016 Santika Premiere Dyandra Medan Raih Prestasi Pada HUT ke­71 RI Telkomsel Tantang Generasi Muda Lahirkan Solusi Smart City dan Perdesaan Meriahkan HUT ke­71 RI, Pertamina Bagi Pertalite Gratis Acer Predator Gelar Counter Strike GO Competition 2016 5/25/2017 Komunitas FunTalk Indonesia Gelar Aksi Bersih Istana Maimoon. Togar Manik, salah satu pendiri Komunitas ini mengatakan bahwa Istana Maimoon sangat ramai dikunjungi oleh wisatawan lokal pada hari Sabtu, sehingga kemungkinan besar banyak sampah kering yang berserakan seperti botol air mineral, bungkus rokok dan bungkus jajanan ringan. “Untuk itu kami ada disana membersihkan sampah dan membuangnya pada tempatnya. Belasan anak muda dari berbagai universitas yang ikut aksi ini, senang melakukannya karena selain aksi sosial juga tidak mengganggu aktifitas kerja atau kuliah karena diadakan hari Sabtu sore,” kata Togar.<br><br>Bagian Kerumahtanggaan Istana Maimoon sangat menyambut baik saat Funtalk Indonesia menyampaikan misi ini melalui surat resmi, dan berharap bisa berjalan secara berkesinambungan. Berkontribusi secara real terhadap lingkungan dengan cara membersihkan sampah yang dibuang sembarangan, mudah mudahan akan menjadi contoh untuk pengeunjung Istana Maimoon yang lainnya untuk membuang sampah pada tempat yang sudah disediakan sehingga kelestarian Heritage Sumatera Utara ini bisa lestari. FunTalk Indonesia bukan hanya sekedar aksi bersih bersih, akan banyak agenda yang akan dijalankan, misalnya minggu ke II Februari nanti akan bersama teman teman mahasiswa di Sidikalang untuk ikuta ambil peran menyampaikan betapa pentingnya bersosialisasi saat masih kuliah dan juga memanfaatkan Media Online / Sosial Media dalam kehidupan sehari hari untuk kepentingan perkuliahan dan kepentingan bisnis. Dan banyak agenda lain yang berhubungan dengan Talkshow/Ngobrol Bijak ­Focus Group Discussion dan Aktifitas Komunitas. “Untuk membawa perubahan kearah yang lebih bijak dan pintar dibutuhkan wadah, maka bergabung disetiap aktifitas Funtalk Indonesia adalah pilihan yang tepat,” jelas Dara, selaku Public Relation dari FunTalk Indonesia <br></span><br></p>', 'Komunitas Fun Talk Indonesia Gelar Aksi Bersih Istana Maimoon_20180212111319.jpg', '2017-05-17 01:14:11', '2017-05-17 01:14:11', 337, '2018-02-15 02:25:58', 1, 'Kabarmedan.com'),
(8, 'Jual Beli Barang Kos di Fun Garage Sale Bisa Ikut Beramal', '<p><strong>TRIBUN-MEDAN.com, MEDAN -</strong> Mau ikut beramal dengan lingkungan sekitar? Komunitas <a href=\"http://medan.tribunnews.com/tag/fun-talk\" title=\"Fun Talk\">Fun Talk</a> Indonesia mengajak Anda untuk jual beli sekaligus beramal untuk lingkungan sekitar melalui Fun Garage Sale.</p><p>Founder <a href=\"http://medan.tribunnews.com/tag/fun-talk\" title=\"Fun Talk\">Fun Talk</a> Indonesia, <a href=\"http://medan.tribunnews.com/tag/togar-manik\" title=\"Togar Manik\">Togar Manik</a> mengatakan latar belakang kegiatan Fun Garage Sale karena pihaknya mempunyai ide untuk membantu para <a href=\"http://medan.tribunnews.com/tag/mahasiswa\" title=\"mahasiswa\">mahasiswa</a>, alumni, dan umum.</p><p>\"Mahasiswa baru yang tinggal di indekos biasanya mempersiapkan kelengkapan barang-barang. Lalu alumni atau pekerja yang akan kembali ke kampung halaman kebingungan menjual barang. Momen inilah yang kami sambut untuk membantu teman-teman <a href=\"http://medan.tribunnews.com/tag/mahasiswa\" title=\"mahasiswa\">mahasiswa</a> dan umum sekaligus untuk beramal,\" kata Togar kepada <em><a href=\"http://www.tribun-medan.com\">www.tribun-medan.com</a></em>, Rabu (31/8/2016).</p><p>Dirinya mengatakan komunitasnya selalu aktif mengadakan kegiatan-kegiatan sosial. Pada moment ini juga Ia dan teman-temannya bergerak untuk menjadi perantara jual beli sekaligus beramal.</p><p>Ia menjelaskan barang yang dijual nantinya akan dinaikkan 10 persen untuk disumbangkan untuk anak-anak di Sekolah Dasar (SD) yang membutuhkan.</p><p>\"Penjual nanti tentukan harga berapa, kita beli, setelah itu kami jual dengan harga yang akan dinaikkan sedikit, misalnya harga yang kita beli Rp 60 ribu, nanti kita jual Rp 70 ribu, keuntungan Rp 10 ribu itu yang akan disumbangkan,\" jelasnya.</p><p>Anda yang berminat bisa mengantarkan barangnya atau menghubungi <a href=\"http://medan.tribunnews.com/tag/fun-talk\" title=\"Fun Talk\">Fun Talk</a> Indonesia, pada 29 Agustus hingga 4 September 2016. \"Kami buka dari Jam 2 siang hingga 8 malam, penerimaan barang bisa ke Fun Lab atau melalui Line <em>@funtalkindonesia</em>, tinggal kirim foto dan harga, nanti kami seleksi, kalau sesuai dan barang masih layak pakai baru kami beli,\" katanya.</p><p>Seraya mengatakan penerimaan barang berakhir tanggal 4 Sepetember 2016. Sebab pada 5-10 September 2016 akan dilakukan penjualan di Fun Lab, Jalan Pembangunan Nomor 40. \"Kami undang <a href=\"http://medan.tribunnews.com/tag/mahasiswa\" title=\"mahasiswa\">mahasiswa</a> baru dan teman-teman yang lagi cari kebutuhan barang kostan untuk membeli di tanggal 5-10 September,\" tawarnya.</p><p>Togar menyatakan penjualan dan pembelian barang-barang tanpa biaya registrasi, bahkan penjemputan barang akan dilakukan secara gratis untuk yang berada di sekitar kampus Universitas Sumatera Utara.</p><p>Togar mengatakan untuk saat ini sudah terkumpul barang-barang seperti<em> magic com,</em> dispenser, setrika, kompor, meja, dan lain-lain.</p>', 'Jual Beli Barang Kos di Fun Garage Sale Bisa Ikut Beramal_20170526014002.jpg', '2017-05-26 13:40:02', '2017-05-26 13:40:02', 300, '2018-02-15 02:26:02', 1, 'Tribun Medan');

-- --------------------------------------------------------

--
-- Table structure for table `newsletter`
--

CREATE TABLE `newsletter` (
  `id` int(11) NOT NULL,
  `name` varchar(150) DEFAULT NULL,
  `email` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `program`
--

CREATE TABLE `program` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `image` text NOT NULL,
  `display_status` tinyint(1) NOT NULL,
  `order_program` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `program`
--

INSERT INTO `program` (`id`, `name`, `description`, `image`, `display_status`, `order_program`) VALUES
(1, 'KELAS KECE', 'Pelatihan berkelanjutan dengan mengundang para mereka-mereka yang berpengalaman di bidangnya masing-masing untuk langsung berbagi ilmu dan mempraktekkannya langsung bersama kamu', 'KELAS KECE_20170516122531.png', 1, 1),
(2, 'TANPA BATAS', 'Program open recruitment volunteer Fun Talk Indonesia, dimana kami membuka kesempatan kepada anak muda Kota Medan untuk menjadi tim Fun Talk Indonesia.', 'TANPA BATAS_20170516122537.png', 1, 2),
(3, 'FUN GARAGE SALE', 'Kegiatan mewadahi kawan-kawan yang ingin menjual atau membeli barang bekas dengan harga terjangkau, sekaligus bisa donasi loh.', 'FUN GARAGE SALE_20170516122600.png', 1, 4),
(4, 'SABTU PINTAR', 'Mini talkshow menghadirkan tokoh-tokoh kece nan inspiratif untuk berbagi ilmu dan pengalaman.\r\nSiapa saja boleh berbagi cerita dan pengalaman, termasuk kamu!', 'SABTU PINTAR_20170516122606.png', 1, 5);

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `name`) VALUES
(1, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`, `role_id`) VALUES
(1, 'admin', '', '$2y$13$ZotH47dWEqE7sm0q2P0YEeBWZdRNALg5f8fLN9HhY1pRHnTIE9sC2', NULL, '', 10, 1493355615, 1493355615, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`id`),
  ADD KEY `configuration_type` (`configuration_type`);

--
-- Indexes for table `configuration_type`
--
ALTER TABLE `configuration_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `newsletter`
--
ALTER TABLE `newsletter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `program`
--
ALTER TABLE `program`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`),
  ADD KEY `role_id` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `config`
--
ALTER TABLE `config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `configuration_type`
--
ALTER TABLE `configuration_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `newsletter`
--
ALTER TABLE `newsletter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `program`
--
ALTER TABLE `program`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `config`
--
ALTER TABLE `config`
  ADD CONSTRAINT `config_ibfk_1` FOREIGN KEY (`configuration_type`) REFERENCES `configuration_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
