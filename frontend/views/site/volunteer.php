
<div style="overflow:hidden;">
<div id="header">
    <header class="header" style="position: relative;"><!--header-start-->
      <div id="bg-opacity-header" style="position: absolute;width: 100%;height: 100%;background-color: rgba(0, 0, 0, 0.7);z-index: 1; "></div>
      <div class="container-fluid img-opacity-header" style="height: 100%; width: 100%;">
            <div id="myCarousel" class="carousel slide" data-ride="carousel" style="height: 90vh; width: 100%;">
  			<!-- Indicators -->
  			<ol class="carousel-indicators">
  			  <?php 
                    foreach ($banner as $key => $bn) {
                 ?>
                    <?php if($key==0){?>
                        <li data-target="#myCarousel" data-slide-to="<?php echo $key; ?>" class="active"></li>
                    <?php }else{?>
                        <li data-target="#myCarousel" data-slide-to="<?php echo $key; ?>"></li>
                    <?php } ?>

                 <?php } ?>
  			</ol>

  			<!-- Wrapper for slides -->
  			<div class="carousel-inner" role="listbox" style="height: 100%; width: 100%;">

  			  <?php 
                    foreach ($banner as $key => $bn) {
                 ?>
                    <?php if($key==0){?>
                     <div class="item active">
                        <img src="<?=Yii::$app->urlManager->createUrl('/uploads/'.$bn->image)?>" alt="">
                    </div>
                    <?php }else{?>
                    <div class="item">
                        <img src="<?=Yii::$app->urlManager->createUrl('/uploads/'.$bn->image)?>" alt="">
                    </div>      
                    <?php } ?>

                 <?php } ?>
  		  
  			</div>
  		  </div>		  
    </div>
    <div>
      <div class="caption">
      <?php echo $caption->value; ?>
      </div>
    </div>
	</header><!--header-end-->
</div>
</div>

<section class="main-section team" id="team"><!--main-section team-start-->
	<div class="container">
        <h2 class="text-orange"><?php echo $content_title->value; ?></h2>
        <h6><?php echo $content_text->value; ?></h6>
        <div class="team-leader-block clearfix" style="padding: 0;">
          <?php foreach ($volunteer as $key => $value) { ?>
          <div class="team-leader-box team-leader-volunteer">
            <div class="team-leader wow fadeInDown delay-03s"> 
              <img src="<?=Yii::$app->urlManager->createUrl('/uploads/'.$value->photo)?>" alt="">
            </div>
            <h3 class="wow fadeInDown delay-03s text-orange"><?php echo $value->name; ?></h3>
            <span class="wow fadeInDown delay-03s"><?php echo $value->position; ?></span>
            <small class="wow fadeInDown delay-06s"><?php echo $value->description; ?></small>        
          </div>
          <?php } ?>
			
        </div>
    </div>
</section><!--main-section team-end-->
