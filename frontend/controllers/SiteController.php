<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use common\models\Program;
use common\models\Banner;
use common\models\Config;
use common\models\News;
use common\models\Member;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $this->view->title = Config::findOne(['key_config'=>'g_title'])->value;
        $banner=Banner::find()
                ->where(['configuration_type'=>1])
                ->orWhere(['configuration_type'=>3])
                ->orderBy(['order_banner'=>SORT_ASC])
                ->all();
        $program=Program::find()
                ->where(['display_status'=>1])
                ->orderBy(['order_program'=>SORT_ASC])
                ->all();
        $caption=Config::findOne(['key_config'=>'brd_caption']);
        $news=News::find()
              ->where(['display_status'=>1])
              ->orderBy(['date_created'=>SORT_DESC])
              ->limit(6)
              ->all();


        return $this->render('index',[
                'banner'=>$banner,
                'program'=>$program,
                'caption'=>$caption,
                'news'=>$news,
            ]);
    }

    public function actionFunlabs()
    {
        $this->view->title = Config::findOne(['key_config'=>'g_title'])->value;

        $banner=Banner::find()
        ->where(['configuration_type'=>1])
        ->orWhere(['configuration_type'=>8])
        ->orderBy(['order_banner'=>SORT_ASC])
        ->all();
        $caption=Config::findOne(['key_config'=>'flb_caption']);
        $logo=Config::findOne(['key_config'=>'flb_logo']);
        $address=Config::findOne(['key_config'=>'flb_address']);
        return $this->render('funlabs',[
                'banner'=>$banner,
                'caption'=>$caption,
                'logo'=>$logo,
                'address'=>$address,
            ]);

    }

    public function actionNewsletterpost()
    {
        $newsletter=new \common\models\NewsLetter();
        $newsletter->name=$_POST['name'];
        $newsletter->email=$_POST['email'];
        $newsletter->save();
        Yii::$app->session->setFlash('success', "Terimakasih telah berpartisipasi!");
        return $this->redirect(\Yii::$app->request->referrer);



    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        $this->view->title = Config::findOne(['key_config'=>'g_title'])->value.' | Budaya';

        $banner=Banner::find()
                ->where(['configuration_type'=>1])
                ->orWhere(['configuration_type'=>2])
                ->orderBy(['order_banner'=>SORT_ASC])
                ->all();

        $caption=Config::findOne(['key_config'=>'bdy_caption']);
        $content1_title=Config::find()
                ->where(['key_config'=>'bdy_content1_title'])
                ->one();
        $content1_text=Config::find()
                ->where(['key_config'=>'bdy_content1_text'])
                ->one();

        $content2_title=Config::find()
                ->where(['key_config'=>'bdy_content2_title'])
                ->one();
        $content2_text=Config::find()
                ->where(['key_config'=>'bdy_content2_text'])
                ->one();

        $content3_title=Config::find()
                ->where(['key_config'=>'bdy_content3_title'])
                ->one();
        $content3_text=Config::find()
                ->where(['key_config'=>'bdy_content3_text'])
                ->one();
        $content4_title=Config::find()
                ->where(['key_config'=>'bdy_content4_title'])
                ->one();
        $content4_text=Config::find()
                ->where(['key_config'=>'bdy_content4_text'])
                ->one();

        return $this->render('about',[
            'banner'=>$banner,
            'caption'=>$caption,
            'content1_title'=>$content1_title,
            'content1_text'=>$content1_text,
            'content2_title'=>$content2_title,
            'content2_text'=>$content2_text,
            'content3_title'=>$content3_title,
            'content3_text'=>$content3_text,
            'content4_title'=>$content4_title,
            'content4_text'=>$content4_text,
            ]);
    }

    public function actionTimKami()
    {
         $this->view->title = Config::findOne(['key_config'=>'g_title'])->value.' | Tim Kami';
         $banner=Banner::find()
         ->where(['configuration_type'=>1])
         ->orWhere(['configuration_type'=>4])
         ->orderBy(['order_banner'=>SORT_ASC])
         ->all();
         $content_title=Config::find()
                ->where(['key_config'=>'tk_content_title'])
                ->one();
         $content_text=Config::find()
                ->where(['key_config'=>'tk_content_text'])
                ->one();
         $caption=Config::findOne(['key_config'=>'tk_caption']);


         $team=Member::find()
                ->where(['status'=>1])
                ->all();
         return $this->render('tim_kami',[
            'banner'=>$banner,
            'caption'=>$caption,
            'content_title'=>$content_title,
            'content_text'=>$content_text,
            'team'=>$team,
            ]);

    }

    public function actionPartner()
    {
       $this->view->title = Config::findOne(['key_config'=>'g_title'])->value.' | Partner';
       $banner=Banner::find()
       ->where(['configuration_type'=>1])
       ->orWhere(['configuration_type'=>5])
       ->orderBy(['order_banner'=>SORT_ASC])
       ->all();
        $volunteer=Member::find()
                ->where(['status'=>2])
                ->all();
        $caption=Config::findOne(['key_config'=>'pt_caption']);
        $content_title=Config::find()
                ->where(['key_config'=>'pt_content_title'])
                ->one();
        $content_text=Config::find()
                ->where(['key_config'=>'pt_content_text'])
                ->one();

        return $this->render('volunteer',[
            'banner'=>$banner,
            'caption'=>$caption,
            'content_title'=>$content_title,
            'content_text'=>$content_text,
            'volunteer'=>$volunteer,
            ]);


    }

    public function actionPublikasi()
    {
        $this->view->title = Config::findOne(['key_config'=>'g_title'])->value.' | Publikasi';
       $banner=Banner::find()
       ->where(['configuration_type'=>1])
       ->orWhere(['configuration_type'=>6])
       ->orderBy(['order_banner'=>SORT_ASC])
       ->all();
        $caption=Config::findOne(['key_config'=>'pub_caption']);
        $content_title=Config::find()
                ->where(['key_config'=>'pub_content_title'])
                ->one();
        $news=News::find()
                ->where(['display_status'=>1])
                ->orderBy(['date_created'=>SORT_DESC])
                ->all();

        return $this->render('publikasi',[
            'banner'=>$banner,
            'caption'=>$caption,
            'content_title'=>$content_title,
            'news'=>$news,
            ]);

    }

    public function actionCeritaKami()
    {
        $this->view->title = Config::findOne(['key_config'=>'g_title'])->value.' | Cerita Kami';
        $banner=Banner::find()
        ->where(['configuration_type'=>1])
        ->orWhere(['configuration_type'=>7])
        ->orderBy(['order_banner'=>SORT_ASC])
        ->all();
        $caption=Config::findOne(['key_config'=>'ck_caption']);
        $content_title=Config::find()
        ->where(['key_config'=>'ck_content_title'])
        ->one();
        $news=News::find()
        ->where(['display_status'=>1])
        ->orderBy(['date_created'=>SORT_DESC])
        ->all();
        return $this->render('story',[
            'banner'=>$banner,
            'caption'=>$caption,
            'content_title'=>$content_title,
            'news'=>$news,
            ]);
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }



    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
}
