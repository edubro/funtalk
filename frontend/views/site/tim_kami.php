
<div style="overflow:hidden;">
<div id="header">
    <header class="header" style="position: relative;"><!--header-start-->
      <div id="bg-opacity-header" style="position: absolute;width: 100%;height: 100%;background-color: rgba(0, 0, 0, 0.7);z-index: 1; "></div>
      <div class="container-fluid img-opacity-header" style="height: 100%; width: 100%;">
            <div id="myCarousel" class="carousel slide" data-ride="carousel" style="height: 90vh; width: 100%;">
  			<!-- Indicators -->
  			<ol class="carousel-indicators">
  			   <?php
                    foreach ($banner as $key => $bn) {
                 ?>
                    <?php if($key==0){?>
                        <li data-target="#myCarousel" data-slide-to="<?php echo $key; ?>" class="active"></li>
                    <?php }else{?>
                        <li data-target="#myCarousel" data-slide-to="<?php echo $key; ?>"></li>
                    <?php } ?>

                 <?php } ?>

  			</ol>

  			<!-- Wrapper for slides -->
  			<div class="carousel-inner" role="listbox" style="height: 100%; width: 100%;">

  			  <?php
                    foreach ($banner as $key => $bn) {
                 ?>
                    <?php if($key==0){?>
                     <div class="item active">
                        <img src="<?=Yii::$app->urlManager->createUrl('/uploads/'.$bn->image)?>" alt="">
                    </div>
                    <?php }else{?>
                    <div class="item">
                        <img src="<?=Yii::$app->urlManager->createUrl('/uploads/'.$bn->image)?>" alt="">
                    </div>
                    <?php } ?>

                 <?php } ?>

  			</div>
  		  </div>
    </div>
    <div>
      <div class="caption">
      <?php echo $caption->value; ?>
      </div>
    </div>
	</header><!--header-end-->
</div>
</div>

<section class="main-section team" id="team"><!--main-section team-start-->
	<div class="container">
        <h2 class="text-orange"><?php echo $content_title->value; ?></h2>
        <h6><?php echo $content_text->value; ?></h6>
        <div class="team-leader-block clearfix" style="padding: 0;">
        <?php foreach ($team as $key => $tm) {?>
        	<div class="item-leader-box">
					<img src="<?=Yii::$app->urlManager->createUrl('/uploads/'.$tm->photo)?>" alt="">
					<br/><br/>
					<h3 class="wow fadeInDown delay-03s text-orange">
						<a href="" data-toggle="modal" data-target="#jimmy" data-name="<?php echo $tm->name; ?>" data-image="<?=Yii::$app->urlManager->createUrl('/uploads/'.$tm->photo)?>" data-desc="<?php echo $tm->description; ?>">
							<?php echo $tm->name; ?>
						</a>
					</h3>
					<span class="wow fadeInDown delay-03s"><?php echo $tm->position; ?></span>
					<br/><br/>
					<?php if($tm->facebook!=''){ ?>
					<a href="<?php echo $tm->facebook; ?>" target="_blank">
					<i class="fa fa-facebook fa-fb" aria-hidden="true"></i>
					</a>
					<?php } ?>

					<?php if($tm->twitter!=''){ ?>
					<a href="<?php echo $tm->twitter; ?>" target="_blank">
					<i class="fa fa-twitter fa-tw" aria-hidden="true"></i>
					</a>
					<?php } ?>
					<?php if($tm->whatsapp!=''){ ?>
					<a href="<?php echo $tm->whatsapp; ?>" target="_blank">
					<i class="fa fa-whatsapp fa-wa" aria-hidden="true"></i>
					</a>
					<?php } ?>

					<?php if($tm->pinterest!=''){ ?>
					<a href="<?php echo $tm->pinterest; ?>" target="_blank">
					<i class="fa fa-pinterest fa-pin" aria-hidden="true"></i>
					</a>
					<?php } ?>

					<?php if($tm->email!=''){ ?>
					<a href="<?php echo $tm->email; ?>" target="_blank">
					<i class="fa fa-envelope" aria-hidden="true"></i>
					</a>
					<?php } ?>
				</div>
				<?php if(($key+1)==count($team)||($key+1)%4==0){?>
					</div>
				<?php } ?>
        	<?php if(($key+1)%4==0&&($key+1)!=count($team)){ ?>
        		
						<div class="team-leader-block clearfix" style="padding: 0;">
        	<?php } ?>

        <?php } ?>

        <!--
		<div class="team-leader-block clearfix" style="padding: 0;">
				<div class="item-leader-box">
					<img src="img/jimmy.png" alt="">
					<br/><br/>
					<h3 class="wow fadeInDown delay-03s text-orange"><a href="" data-toggle="modal" data-target="#jimmy">Jimmy</a></h3>
					<span class="wow fadeInDown delay-03s">Founder & Konseptor</span>
					<br/><br/>
					<i class="fa fa-facebook fa-fb" aria-hidden="true"></i>
					<i class="fa fa-twitter fa-tw" aria-hidden="true"></i>
					<i class="fa fa-whatsapp fa-wa" aria-hidden="true"></i>
					<i class="fa fa-pinterest fa-pin" aria-hidden="true"></i>
					<i class="fa fa-envelope" aria-hidden="true"></i>
				</div>

				<div class="item-leader-box">
					<img src="img/togar.png" alt="">
					<br/><br/>
					<h3 class="wow fadeInDown delay-03s text-orange">Togar</h3>
					<span class="wow fadeInDown delay-03s">Human Resources</span>
					<br/><br/>
					<i class="fa fa-facebook fa-fb" aria-hidden="true"></i>
					<i class="fa fa-twitter fa-tw" aria-hidden="true"></i>
					<i class="fa fa-whatsapp fa-wa" aria-hidden="true"></i>
					<i class="fa fa-pinterest fa-pin" aria-hidden="true"></i>
					<i class="fa fa-envelope" aria-hidden="true"></i>
				</div>

				<div class="item-leader-box">
					<img src="img/tosi.png" alt="">
					<br/><br/>
					<h3 class="wow fadeInDown delay-03s text-orange">Tosi</h3>
					<span class="wow fadeInDown delay-03s">Secretary</span>
					<br/><br/>
					<i class="fa fa-facebook fa-fb" aria-hidden="true"></i>
					<i class="fa fa-twitter fa-tw" aria-hidden="true"></i>
					<i class="fa fa-whatsapp fa-wa" aria-hidden="true"></i>
					<i class="fa fa-pinterest fa-pin" aria-hidden="true"></i>
					<i class="fa fa-envelope" aria-hidden="true"></i>
				</div>

				<div class="item-leader-box">
					<img src="img/bernard.png" alt="">
					<br/><br/>
					<h3 class="wow fadeInDown delay-03s text-orange">Bernard</h3>
					<span class="wow fadeInDown delay-03s">Photographer</span>
					<br/><br/>
					<i class="fa fa-facebook fa-fb" aria-hidden="true"></i>
					<i class="fa fa-twitter fa-tw" aria-hidden="true"></i>
					<i class="fa fa-whatsapp fa-wa" aria-hidden="true"></i>
					<i class="fa fa-pinterest fa-pin" aria-hidden="true"></i>
					<i class="fa fa-envelope" aria-hidden="true"></i>
				</div>
        </div>
		<div class="team-leader-block clearfix" style="padding: 0;">
				<div class="item-leader-box">
					<img src="img/yuni.png" alt="">
					<br/><br/>
					<h3 class="wow fadeInDown delay-03s text-orange">Yuni</h3>
					<span class="wow fadeInDown delay-03s">Content Writer</span>
					<br/><br/>
					<i class="fa fa-facebook fa-fb" aria-hidden="true"></i>
					<i class="fa fa-twitter fa-tw" aria-hidden="true"></i>
					<i class="fa fa-whatsapp fa-wa" aria-hidden="true"></i>
					<i class="fa fa-pinterest fa-pin" aria-hidden="true"></i>
					<i class="fa fa-envelope" aria-hidden="true"></i>
				</div>

				<div class="item-leader-box">
					<img src="img/dhian.png" alt="">
					<br/><br/>
					<h3 class="wow fadeInDown delay-03s text-orange">Dhian</h3>
					<span class="wow fadeInDown delay-03s">Public Relations</span>
					<br/><br/>
					<i class="fa fa-facebook fa-fb" aria-hidden="true"></i>
					<i class="fa fa-twitter fa-tw" aria-hidden="true"></i>
					<i class="fa fa-whatsapp fa-wa" aria-hidden="true"></i>
					<i class="fa fa-pinterest fa-pin" aria-hidden="true"></i>
					<i class="fa fa-envelope" aria-hidden="true"></i>
				</div>

				<div class="item-leader-box">
					<img src="img/eko.png" alt="">
					<br/><br/>
					<h3 class="wow fadeInDown delay-03s text-orange">Eko</h3>
					<span class="wow fadeInDown delay-03s">Project Manager</span>
					<br/><br/>
					<i class="fa fa-facebook fa-fb" aria-hidden="true"></i>
					<i class="fa fa-twitter fa-tw" aria-hidden="true"></i>
					<i class="fa fa-whatsapp fa-wa" aria-hidden="true"></i>
					<i class="fa fa-pinterest fa-pin" aria-hidden="true"></i>
					<i class="fa fa-envelope" aria-hidden="true"></i>
				</div>

				<div class="item-leader-box">
					<img src="img/adit.png" alt="">
					<br/><br/>
					<h3 class="wow fadeInDown delay-03s text-orange">Adit</h3>
					<span class="wow fadeInDown delay-03s">Videographer</span>
					<br/><br/>
					<i class="fa fa-facebook fa-fb" aria-hidden="true"></i>
					<i class="fa fa-twitter fa-tw" aria-hidden="true"></i>
					<i class="fa fa-whatsapp fa-wa" aria-hidden="true"></i>
					<i class="fa fa-pinterest fa-pin" aria-hidden="true"></i>
					<i class="fa fa-envelope" aria-hidden="true"></i>
				</div>
        </div>
		<div class="team-leader-block clearfix" style="padding: 0;">
				<div class="item-leader-box">
					<img src="img/dino.png" alt="">
					<br/><br/>
					<h3 class="wow fadeInDown delay-03s text-orange">Dino</h3>
					<span class="wow fadeInDown delay-03s">Web Master</span>
					<br/><br/>
					<i class="fa fa-facebook fa-fb" aria-hidden="true"></i>
					<i class="fa fa-twitter fa-tw" aria-hidden="true"></i>
					<i class="fa fa-whatsapp fa-wa" aria-hidden="true"></i>
					<i class="fa fa-pinterest fa-pin" aria-hidden="true"></i>
					<i class="fa fa-envelope" aria-hidden="true"></i>
				</div>

				<div class="item-leader-box">
					<img src="img/dosni.png" alt="">
					<br/><br/>
					<h3 class="wow fadeInDown delay-03s text-orange">Dosni</h3>
					<span class="wow fadeInDown delay-03s">General Affair</span>
					<br/><br/>
					<i class="fa fa-facebook fa-fb" aria-hidden="true"></i>
					<i class="fa fa-twitter fa-tw" aria-hidden="true"></i>
					<i class="fa fa-whatsapp fa-wa" aria-hidden="true"></i>
					<i class="fa fa-pinterest fa-pin" aria-hidden="true"></i>
					<i class="fa fa-envelope" aria-hidden="true"></i>
				</div>

				<div class="item-leader-box">
					<img src="img/daud.png" alt="">
					<br/><br/>
					<h3 class="wow fadeInDown delay-03s text-orange">Daud</h3>
					<span class="wow fadeInDown delay-03s">Graphic Designer</span>
					<br/><br/>
					<i class="fa fa-facebook fa-fb" aria-hidden="true"></i>
					<i class="fa fa-twitter fa-tw" aria-hidden="true"></i>
					<i class="fa fa-whatsapp fa-wa" aria-hidden="true"></i>
					<i class="fa fa-pinterest fa-pin" aria-hidden="true"></i>
					<i class="fa fa-envelope" aria-hidden="true"></i>
				</div>
        </div>	 -->
    </div>
</section><!--main-section team-end-->
<div id="jimmy" class="modal modal-no-scroll fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content modal-jimmy">
      <div class="modal-body">
        <div class="col-md-5 col-xs-5">
				<img src="img/jimmy.png" alt="" class="imgmodal">
		</div>
		<div class="col-md-7 col-xs-7">
			<h2 class="text-white text-left text-margin" id="modal-jimmy-name">  </h2>
			<p class="text-white text-modal-description"></p>
		</div>
		<div class="clearfix"></div>
      </div>
    </div>

  </div>
</div>
<?php
$myScript="
	$('a[data-target=#jimmy]').on('click',function(){
		$('#modal-jimmy-name').html($(this).attr('data-name'));
		$('.text-modal-description').html($(this).attr('data-desc'));
		$('.imgmodal').attr('src',$(this).attr('data-image'));

	});

";

$this->registerJs($myScript);
 ?>
