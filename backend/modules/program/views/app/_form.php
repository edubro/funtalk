<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Program */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="program-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'imageFile')->label('Image')->fileInput()->hint('Image format must be *.jpg or *.png') ?>
    <?php if($model->image!=''){ ?>
    <div class="form-group">
        <label class="control-label" ></label>
        <img src="<?=Yii::$app->urlManager->createUrl('../uploads/'.$model->image)?>" style="background-color: #eee;max-width: 250px;" class="img-thumbnail">

        <div class="help-block"></div>
    </div>
    <?php } ?>
    <?= $form->field($model, 'display_status')->dropDownList([1=>'Yes',2=>'No']) ?>

    <?= $form->field($model, 'order_program')->textInput(['type'=>'number']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
