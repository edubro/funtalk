<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'News';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create News', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
     <?= fedemotta\datatables\DataTables::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'content:ntext',
            [
                'attribute' => 'img',
                'format' => 'html',
                'label' => 'Banner Image',
                'value' => function ($data) {
                    return Html::img(\Yii::$app->urlManager->createUrl('../uploads/'.$data['banner_image']),
                        ['width' => '250px']);
                },
            ],
            'date_update',
            'view',
            'last_view',
            [

            'label' => 'Display Status',
            'value' => function ($data) {
                    return $data['display_status']==1?'Yes':'No';
                },
            ],
            'writer',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
