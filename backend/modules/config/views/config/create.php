<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Config */

$this->title = 'Buat Konfigurasi';
$this->params['breadcrumbs'][] = ['label' => 'Configs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="config-create">

	<?php 
		$form='_form';
		if(isset($_GET['content_type']))
		{
			if($_GET['content_type']==2||$_GET['content_type']==3)
			{
				$form='_form2';
			}
		}

	 ?>
    <?= $this->render($form, [
        'model' => $model,
    ]) ?>

</div>
