<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);
$baseUrl='/FUNTALK/administrator';
$frontendUrl='';
return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'name'=>'Dashboard Admin',
    'controllerNamespace' => 'backend\controllers',
    'homeUrl'=>$baseUrl,
    'bootstrap' => ['log'],
    'modules' => [],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-backend',
            'baseUrl'=>$baseUrl,
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ], 
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
    ],
    'modules' => [
        'config' => ['class' => 'backend\modules\config\Module'],
        'news' => ['class' => 'backend\modules\news\Module'],
        'member' => ['class' => 'backend\modules\member\Module'],
        'banner' => ['class' => 'backend\modules\banner\Module'],
        'program' => ['class' => 'backend\modules\program\Module'],
        'redactor'=>[
            'class' => 'yii\redactor\RedactorModule',
            'uploadDir' => '@uploads',
            'uploadUrl' => $frontendUrl.'\uploads',
            'imageAllowExtensions'=>['jpg','png','gif']
        ],


    ],
    'params' => $params,
];
