
<div style="overflow:hidden;">
<div id="header">
    <header class="header" style="position: relative;"><!--header-start-->
      <div id="bg-opacity-header" style="position: absolute;width: 100%;height: 100%;background-color: rgba(0, 0, 0, 0.7);z-index: 1; "></div>
      <div class="container-fluid img-opacity-header" style="height: 100%; width: 100%;">
            <div id="myCarousel" class="carousel slide" data-ride="carousel" style="height: 90vh; width: 100%;">
  			<!-- Indicators -->
  			<ol class="carousel-indicators">
  			  <?php 
                    foreach ($banner as $key => $bn) {
                 ?>
                    <?php if($key==0){?>
                        <li data-target="#myCarousel" data-slide-to="<?php echo $key; ?>" class="active"></li>
                    <?php }else{?>
                        <li data-target="#myCarousel" data-slide-to="<?php echo $key; ?>"></li>
                    <?php } ?>

                 <?php } ?>
  			</ol>

  			<!-- Wrapper for slides -->
  			<div class="carousel-inner" role="listbox" style="height: 100%; width: 100%;">

  			  <?php 
                    foreach ($banner as $key => $bn) {
                 ?>
                    <?php if($key==0){?>
                     <div class="item active">
                        <img src="<?=Yii::$app->urlManager->createUrl('/uploads/'.$bn->image)?>" alt="">
                    </div>
                    <?php }else{?>
                    <div class="item">
                        <img src="<?=Yii::$app->urlManager->createUrl('/uploads/'.$bn->image)?>" alt="">
                    </div>      
                    <?php } ?>

                 <?php } ?>
  		  
  			</div>
  		  </div>		  
    </div>
    <div>
      <div class="caption">
      <?php echo $caption->value; ?>
      </div>
    </div>
	</header><!--header-end-->
</div>
</div>

<section class="main-section service" id="service"><!--main-section-start-->
	<div class="container">
    	<h2 class="text-padding-h2 wow fadeInUp delay-02s text-orange text-left"><?php echo $content_title->value; ?></h2>
		<div class="col-md-12 col-xs-12 wow fadeInUp delay-03s">
			<h4 class="text-padding-h4 text-black text-left text-orange"><?php echo $news->title; ?></h4>
      <?php $urlImage=\Yii::$app->urlManager->createUrl('/uploads/'.$news->banner_image) ?>
      <div class="image-photo-news bg-photo-1" style="background:url('<?php echo $urlImage; ?>');"></div>		
			<h5 class="text-padding-h5 text-black">
				<?php echo $news->content; ?></h5>  
				<p style="margin: 20px 0;"><b style="color: #f0ad4e;"><small>Penulis: <?php echo $news->writer; ?></small></b></p>
				<div class="addthis_inline_share_toolbox"></div>
				<br>
				<a href="<?php echo \Yii::$app->request->referrer; ?>" class="orange-button">Kembali</a>



		</div>		
		<div class="clearfix"></div>
		<br/>
		<hr/>
	</div>
</section><!--main-section-end-->

<section class="main-section story" id="Portfolio"><!--main-section-start-->
	<div class="container">
	<h2 class="text-padding-h2 text-orange wow fadeInUp delay-01s">Cerita Kami</h2>	
		<?php foreach ($ot_news as $key => $value) {?>
		<div class="col-md-4 col-xs-12 no-padding-left">
			<a href="<?php echo \Yii::$app->urlManager->createUrl(['news']).'/'.$value->id.'/'.$value->title; ?>">
				<?php $urlImage=\Yii::$app->urlManager->createUrl('/uploads/'.$value->banner_image) ?>
				<div class="image-photo bg-photo-1" style="background:url('<?php echo $urlImage; ?>');"></div></a>
			<h3 class="text-black text-capitalize"><?php echo $value->title; ?></h3>
			<h6 class="text-black text-capitalize text-left">Oleh : <?php echo $value->writer; ?></h6>
			
		</div>
		<?php } ?>
		
	</div>
</section>
<!--main-section-end-->

<!-- Go to www.addthis.com/dashboard to customize your tools --> <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5902aaacf511d511"></script> 
