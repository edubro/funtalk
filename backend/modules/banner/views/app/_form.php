<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Banner */
/* @var $form yii\widgets\ActiveForm */
?>
<?php 
$configType= \common\models\ConfigurationType::find()->asArray()
        ->all();
$configTypeArray = yii\helpers\ArrayHelper::map(
        	$configType, 'id', 'name');
 ?>
<div class="banner-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'imageFile')->label('Photo')->fileInput()->hint('Image format must be *.jpg or *.png') ?>
    <?php if($model->image!=''){ ?>
    <div class="form-group">
        <label class="control-label" ></label>
        <img src="<?=Yii::$app->urlManager->createUrl('../uploads/'.$model->image)?>" style="background-color: #eee;max-width: 250px;" class="img-thumbnail">

        <div class="help-block"></div>
    </div>
    <?php } ?>
    <?= $form->field($model, 'configuration_type')->dropDownList($configTypeArray) ?>

    <?= $form->field($model, 'order_banner')->textInput(['type'=>'number']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
