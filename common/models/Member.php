<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "member".
 *
 * @property integer $id
 * @property string $name
 * @property string $position
 * @property string $description
 * @property string $photo
 * @property integer $status
 * @property string $facebook
 * @property string $twitter
 * @property string $whatsapp
 * @property string $pinterest
 * @property string $email
 */
class Member extends \yii\db\ActiveRecord
{
    public $imageFile;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'member';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'position', 'description', 'status'], 'required'],
            [['description', 'photo', 'facebook', 'twitter', 'whatsapp', 'pinterest', 'email'], 'string'],
            [['status'], 'integer'],
            [['name', 'position'], 'string', 'max' => 150],
            [['imageFile'],'file'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'position' => 'Position',
            'description' => 'Description',
            'photo' => 'Photo',
            'status' => 'Status',
            'facebook' => 'Facebook',
            'twitter' => 'Twitter',
            'whatsapp' => 'Whatsapp',
            'pinterest' => 'Pinterest',
            'email' => 'Email',
        ];
    }
}
