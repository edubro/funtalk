<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\News */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="news-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'imageFile')->label('Banner Image')->fileInput()->hint('Image format must be *.jpg or *.png') ?>
    <?php if($model->banner_image!=''){ ?>
    <div class="form-group">
        <label class="control-label" ></label>
        <img src="<?=Yii::$app->urlManager->createUrl('../uploads/'.$model->banner_image)?>" style="background-color: #eee;max-width: 250px;" class="img-thumbnail">

        <div class="help-block"></div>
    </div>
    <?php } ?>

    <?= $form->field($model, 'content')->widget(\yii\redactor\widgets\Redactor::className(), [
            'clientOptions' => [
                            'toolbarOverflow' => true,
                            'plugins' => ['format', 'bold', 'italic', 'fontfamily', 'fontsize', 'fontcolor', 'fullscreen', 'deleted', 'lists', 'image', 'file', 'link', 'horizontalrule', 'font']
                        ]

    ]) ?>



<!--     <?= $form->field($model, 'banner_image')->textarea(['rows' => 6]) ?>
 -->
                        
    <?= $form->field($model, 'display_status')->dropDownList([1=>'Yes',2=>'No']) ?>

    <?= $form->field($model, 'writer')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
