<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property string $title
 * @property string $content
 * @property string $banner_image
 * @property string $date_created
 * @property string $date_update
 * @property integer $view
 * @property string $last_view
 * @property integer $display_status
 * @property string $writer
 */
class News extends \yii\db\ActiveRecord
{

    public $imageFile;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'content', 'banner_image', 'writer'], 'required'],
            [['content', 'banner_image'], 'string'],
            [['date_created', 'date_update', 'last_view'], 'safe'],
            [['view', 'display_status'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['writer'], 'string', 'max' => 150],
            [['imageFile'],'file'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'content' => 'Content',
            'banner_image' => 'Banner Image',
            'date_created' => 'Date Created',
            'date_update' => 'Date Update',
            'view' => 'View',
            'last_view' => 'Last View',
            'display_status' => 'Display Status',
            'writer' => 'Writer',
        ];
    }
}
