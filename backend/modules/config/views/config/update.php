<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Config */

$this->title = 'Perbaharui Konfigurasi';
$this->params['breadcrumbs'][] = ['label' => 'Configs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="config-update">
	<?php 
		$form='_form';
		

		if(isset($_GET['content_type']))
		{
			if($_GET['content_type']==1||$_GET['content_type']==4)
			{
				$form='_form';
			}else{
				$form='_form2';
			}
		}
		elseif($model->content_type==2||$model->content_type==3)
		{
			$form='_form2';
		}


		

	 ?>
    <?= $this->render($form, [
        'model' => $model,
    ]) ?>

</div>
