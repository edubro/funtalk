<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Member */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="member-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'position')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->widget(\dosamigos\tinymce\TinyMce::className()) ?>

    <?= $form->field($model, 'imageFile')->label('Photo')->fileInput()->hint('Image format must be *.jpg or *.png') ?>
    <?php if($model->photo!=''){ ?>
    <div class="form-group">
        <label class="control-label" ></label>
        <img src="<?=Yii::$app->urlManager->createUrl('../uploads/'.$model->photo)?>" style="background-color: #eee;max-width: 250px;" class="img-thumbnail">

        <div class="help-block"></div>
    </div>
    <?php } ?>

    <?= $form->field($model, 'status')->dropDownList([1=>'Tim',2=>'Partner']) ?>

    <?= $form->field($model, 'facebook')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'twitter')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'whatsapp')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pinterest')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
