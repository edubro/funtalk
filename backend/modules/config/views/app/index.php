<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\web\view;
use yii\web\Request;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ConfigSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Konfigurasi';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="config-index">
<h3><?= $configuration_type->name?></h3>
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
            <?php foreach ($config_types as $key => $ct):?>

            <li class="<?php echo $ct->id==$configuration_type->id?'active':''; ?>">
                <a href="<?php echo yii\helpers\Url::to(['app/index','config_type_id'=>$ct->id]); ?>" >
                    <?=$ct->name?>
                </a>
            </li>
            <?php endforeach; ?>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" >
                <?php if(isset($_SESSION['success'])): ?>
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-check"></i> Alert!</h4>
                    <?php echo $_SESSION['success']; ?>
                </div>
                <?php endif ?>
                <div class="config-form">
                    <?php $form = ActiveForm::begin([
                        'options'=>[
                                    'enctype'=>'multipart/form-data',
                                ],
                        ]); ?>

                   <?php foreach( $config_items as $index => $item ): ?>

                        <?php $item_label='';


                             $item_label=$item->name;



                        ?>
                        <?php if($item->key_config=='flb_address'): ?>
                          <div class="form-group field-config-0-value required">
                            <label class="control-label" for="config-0-value"><?php echo $item->name; ?></label>
                            <input required id="map_autocomplete" type="text" id="config-0-value" class="form-control" name="Config[<?php echo $index; ?>][value]" rows="4" value="<?php echo $item->value; ?>">

                            <div class="help-block"></div>
                          </div>

                        <?php elseif($item->content_type==1):?>
                        <?=$form->field($item, '['.$index.']value')->widget(\yii\redactor\widgets\Redactor::className(), [
            'clientOptions' => [
                            'toolbarOverflow' => true,
                            'plugins' => ['format', 'bold', 'italic', 'fontfamily', 'fontsize', 'fontcolor', 'fullscreen', 'deleted', 'lists', 'image', 'file', 'link', 'horizontalrule', 'font']
                        ]

    ])->label($item_label)?>
                        <?php elseif($item->content_type==2): ?>

                        <?= $form->field($item, '['.$index.']imageFile')->label($item_label)->fileInput()->hint('Image format must be *.jpg or *.png') ?>
                        <div class="form-group">
                            <label class="control-label" ></label>
                            <img src="<?=Yii::$app->urlManager->createUrl('../uploads/'.$item->value)?>" style="background-color: #eee;max-width: 250px;" class="img-thumbnail">

                            <div class="help-block"></div>
                        </div>
                        <?php elseif($item->content_type==3): ?>
                        <?= $form->field($item, '['.$index.']imageFile')->label($item_label)->fileInput()->hint('Video format must be *.mp4 and maximum file size is 50MB') ?>
                        <div class="form-group">
                            <label class="control-label" ></label>
                            <video class="video-responsive" width="30%" controls="">
                                <source src="<?=Yii::$app->urlManager->createUrl('../uploads/'.$item->value)?>" type="video/mp4">
                             </video>
                            <div class="help-block"></div>
                        </div>
                        <?php elseif($item->content_type==4):?>
                            <?= $form->field($item,'['.$index.']value')->label($item_label)->textArea(['rows'=>4])?>
                        <?php endif; ?>
                   <?php endforeach; ?>

                    <?php if(count($config_items)>0){ ?>
                    <div class="form-group">
                    <?= Html::submitButton( 'Update', ['class' => 'btn btn-success',]) ?>
                    </div>
                    <?php } ?>
                    <?php ActiveForm::end(); ?>
                </div>
              </div>
            </div>
            <!-- /.tab-content -->
          </div>
</div>
<?php
$this->registerJsFile('https://maps.googleapis.com/maps/api/js?key=AIzaSyDEJifTz-2J9QyeCN9F45uNcSozkeLqSaI&libraries=places&callback=initAutocomplete');

$myJs="
var autocomplete;
function initAutocomplete() {

       autocomplete = new google.maps.places.Autocomplete((document.getElementById('map_autocomplete')),
           {types: ['establishment','geocode']});



     }

";
$this->registerJs($myJs,View::POS_HEAD);
 ?>
