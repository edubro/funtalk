<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "configuration_type".
 *
 * @property integer $id
 * @property string $name
 * @property integer $order_config_type
 *
 * @property Config[] $configs
 */
class ConfigurationType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'configuration_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['order_config_type'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'order_config_type' => 'Order Config Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConfigs()
    {
        return $this->hasMany(Config::className(), ['configuration_type' => 'id']);
    }
}
