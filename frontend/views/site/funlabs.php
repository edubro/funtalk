
<div style="overflow:hidden;">
<header class="header" id="header"><!--header-start-->
	<div class="container">
		<div class="col-md-5 col-xs-5 no-padding-left" align="left">
			<?php echo $caption->value; ?>
		</div>
		<div class="col-md-7 col-xs-7 no-padding-right" align="right">
			<img src="<?=Yii::$app->urlManager->createUrl('/uploads/'.$logo->value)?>" style="padding: 100px 0;"/>
		</div>
    </div>
</div>
</header><!--header-end-->

<section class="main-section team" id="team"><!--main-section team-start-->
	<div class="container">
		  <div id="myCarousel" class="carousel slide" data-ride="carousel">
			<!-- Indicators -->
			<ol class="carousel-indicators">
			  <?php
                    foreach ($banner as $key => $bn) {
                 ?>
                    <?php if($key==0){?>
                        <li data-target="#myCarousel" data-slide-to="<?php echo $key; ?>" class="active"></li>
                    <?php }else{?>
                        <li data-target="#myCarousel" data-slide-to="<?php echo $key; ?>"></li>
                    <?php } ?>

                 <?php } ?>
			</ol>

			<!-- Wrapper for slides -->
			<div class="carousel-inner" role="listbox">

			  <?php
                    foreach ($banner as $key => $bn) {
                 ?>
                    <?php if($key==0){?>
                     <div class="item active">
                        <img src="<?=Yii::$app->urlManager->createUrl('/uploads/'.$bn->image)?>" alt="">
                    </div>
                    <?php }else{?>
                    <div class="item">
                        <img src="<?=Yii::$app->urlManager->createUrl('/uploads/'.$bn->image)?>" alt="">
                    </div>
                    <?php } ?>

                 <?php } ?>

			</div>

			<!-- Left and right controls -->
			<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
			  <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			  <span class="sr-only">Previous</span>
			</a>
			<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
			  <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			  <span class="sr-only">Next</span>
			</a>
		  </div>

			<br>
			<!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3981.9017096138655!2d98.64684351387037!3d3.609961351155771!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30312e1009d1848d%3A0x36c91b8ae9a75bf8!2sJl.+Pembangunan+No.40%2C+Helvetia+Tim.%2C+Medan+Helvetia%2C+Kota+Medan%2C+Sumatera+Utara+20117!5e0!3m2!1sen!2sid!4v1481862555550" width="1140" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>		   -->
			<iframe id="funlabs-maps" src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDEJifTz-2J9QyeCN9F45uNcSozkeLqSaI&q=<?php echo $address->value; ?>" width="1140" height="450" frameborder="0" style="border:0; width: 100%;" allowfullscreen></iframe>
    </div>
</section><!--main-section team-end-->
