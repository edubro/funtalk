<?php

namespace backend\modules\config\controllers;
use Yii;
use yii\helpers\ArrayHelper;
use common\models\Config;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use common\models\UploadForm;
use yii\base\Model;

class AppController extends \yii\web\Controller
{
     public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => \yii\filters\VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
     public function actionIndex($config_type_id=1)
    {
        
        // if(Yii::$app->user->isGuest&&(Yii::$app->user->identity->role_id!=2&&Yii::$app->user->identity->role_id!=1))
        // {
        //     return $this->goHome();
        // }
        // if(Yii::$app->user->identity->role_id==2)
        // {
        //     return $this->render('index_mco');
        // }

        $selected_menu='';


        if(isset($_GET['selected_menu']))
        {
            $selected_menu=$_GET['selected_menu'];
        }
        $config_types=\common\models\ConfigurationType::find()
                    ->all();
        $temp_cm=Config::find()
                    ->where(['configuration_type'=>$config_type_id])
                    ->all();
        $configuration_type=\common\models\ConfigurationType::findOne(['id'=>$config_type_id]);

        // $config_menu=[];
        // foreach ($temp_cm as $key => $cm) {
        //     $temp_string=explode('_', $cm->key_config);
        //     if(count($temp_string)>=2)
        //     {
        //         if(!in_array(ucfirst($temp_string[1]), $config_menu))
        //         {
        //             $config_menu[]=ucfirst($temp_string[1]);
        //         }
        //     }
        // }
        // sort($config_menu);
        // if(count($config_menu)>0 && $selected_menu=='')
        // {
        //     $selected_menu=strtolower($config_menu[0]);
        // }




        $config_items=Config::find()
                    // ->where('key_config like "%_'.$selected_menu.'%"')
                    ->where('configuration_type='.$config_type_id)
                    ->orderBy(['order_config'=>SORT_ASC])
                    ->all();

        if(Model::loadMultiple($config_items, Yii::$app->request->post()) && Model::validateMultiple($config_items))
        {
            $count=0;
            foreach ($config_items as $index => $item) {
                $uploadModel= UploadedFile::getInstance($item,'['.$index.']imageFile');

                if($uploadModel!=null &&($uploadModel->extension=='jpg'||$uploadModel->extension=='png'||$uploadModel->extension=='ico') && $item->content_type==2)
                {
                    $imgName=\Yii::getAlias('@uploads')."/".$item->key_config.'_'.date('Ymdhis').".".$uploadModel->extension;
                    $uploadModel->saveAs($imgName);
                    $item->value=$item->key_config.'_'.date('Ymdhis').".".$uploadModel->extension;
                }elseif($uploadModel!=null &&($uploadModel->extension=='mp4'&&$uploadModel->size<=100000000)&& $item->content_type==3)
                {
                    $videoName=\Yii::getAlias('@uploads')."/vid_".$item->key_config.'_'.date('Ymdhis').".".$uploadModel->extension;
                    $uploadModel->saveAs($videoName);
                    $item->value="vid_".$item->key_config.'_'.date('Ymdhis').".".$uploadModel->extension;
                }

                if ($item->save()){
                    $count++;
                } 
            }
            if($count==count($config_items))
            {
                Yii::$app->session->setFlash('success', "Updated successfully.");
            }else{
                Yii::$app->session->setFlash('failed', "Updated failed.");
            }
        }

        return $this->render('index',[
                // 'config_menu'=>$config_menu,
                'configuration_type'=>$configuration_type,
                'config_items'=>$config_items,
                'config_types'=>$config_types,
                ]);
    }

    


}
