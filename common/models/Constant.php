<?php

namespace common\models;

/**
 * Description of Constant
 *
 * @author Taufik
 */
class Constant {
    const CONTENT_TYPE_CONFIG = [
		    '1' => 'Text',
		    '2' => 'Image',
		    '3'	=> 'Video',
		    '4' => 'Plain Text'
    ];
    
    
    public static function getTestMakerUrl($controllerName,$action,$params = ""){
        return 'localhost:88/work/vip/quiz/web/index.php?r='.$controllerName."/".$action.
            '&'.$params;
    }
}
