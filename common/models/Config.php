<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "config".
 *
 * @property integer $id
 * @property string $key_config
 * @property string $name
 * @property string $value
 * @property integer $configuration_type
 * @property integer $content_type
 * @property integer $order_config
 *
 * @property ConfigurationType $configurationType
 */
class Config extends \yii\db\ActiveRecord
{

    public $imageFile;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'config';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key_config', 'name', 'value', 'configuration_type', 'content_type'], 'required'],
            [['value'], 'string'],
            [['configuration_type', 'content_type', 'order_config'], 'integer'],
            [['key_config'], 'string', 'max' => 255],
            [['name'], 'string', 'max' => 100],
            [['configuration_type'], 'exist', 'skipOnError' => true, 'targetClass' => ConfigurationType::className(), 'targetAttribute' => ['configuration_type' => 'id']],
            [['imageFile'],'file','maxSize' => 1024 * 1024 * 2],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'key_config' => 'Key Config',
            'name' => 'Name',
            'value' => 'Value',
            'configuration_type' => 'Configuration Type',
            'content_type' => 'Content Type',
            'order_config' => 'Order Config',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConfigurationType()
    {
        return $this->hasOne(ConfigurationType::className(), ['id' => 'configuration_type']);
    }
}
