<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/bootstrap.css',
        'css/style.css',
        'css/font-awesome.css',
        'css/responsive.css',
        'css/animate.css',
    ];
    public $js = [
        'js/jquery.1.8.3.min.js',
        'js/bootstrap.js',
        'js/jquery-scrolltofixed.js',
        'js/jquery.easing.1.3.js',
        'js/jquery.isotope.js',
        'js/wow.js',
        'js/classie.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
